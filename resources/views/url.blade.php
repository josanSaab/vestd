<!DOCTYPE html>
<html lang="en">
<head>
    <title>Vestd</title>
    <link rel="shortcut icon" href="https://www.vestd.com/hubfs/favicon.ico">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" />
</head>
<body>
    <div class="container">
    <div class="card">
      <div class="card-header">
    <form method="POST" action="{{ route('shortUrl') }}">
            @csrf
            <div class="input-group mb-3">
              <input type="text" name="long_url" class="form-control" placeholder="Enter URL">

               @error('long_url')
                     <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                @enderror
                <div class="input-group-append">
                    <button class="btn btn-success" type="submit">Short Url</button>
                </div>
            </div>
            @if(isset(Session::get('short')->short_url))
                <a href="{{URL::to('/').'/'. Session::get('short')->short_url }}">{{URL::to('/').'/'. Session::get('short')->short_url }}</a>
            @endif

            @if(isset(Session::get('data')->long_url))
                <a href="{{Session::get('data')->long_url }}">{{Session::get('data')->long_url }}</a>
                <br>
                <p>Count: {{Session::get('data')->count }} </p>
            @endif
     </form>
      </div>
    </div>
    </div>
</body>
</html>
