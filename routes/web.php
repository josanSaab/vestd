<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ShortController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[ShortController::class, 'index'])->name('index');

Route::post('short-url', [ShortController::class, 'shortUrl'])->name('shortUrl');

Route::get('{code}', [ShortController::class, 'getLongLink'])->name('longLink');
