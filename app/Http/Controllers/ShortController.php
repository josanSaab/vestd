<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Short;
use Illuminate\Support\Str;
use Validator;


class ShortController extends Controller
{

    public function index()
    {
        return view('url');
    }

    /**
     * Get the short URL through long URL
     * @param Request
     * @return View
     */

    public function shortUrl(Request $request)
    {
        $validatedData = $request->validate([
            'long_url' => 'required|url',
        ]);
        try {
            $data = Short::where('long_url', $request->long_url)->first();
            if($data){
                return redirect('/')->with(['short' => $data]);
            }
            $input['long_url'] = $request->long_url;
            $input['short_url'] = Str::random(6);
            $data = Short::create($input);
            return redirect('/')->with(['short'=>$data]);
        } catch (\Throwable $th) {
            return redirect('/');
        }
    }

    /**
     * Get the Long URL through Short Link
     * @param short_url
     * @return View
     */
    public function getLongLink($code)
    {
        $data = Short::where('short_url',$code)->first();
            if($data){
                $data->count= $data->count == NULL ? 1 : $data->count+1;
                $data->save();
            }
        return redirect('/')->with(['data' => $data]);
    }

}
