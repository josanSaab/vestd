
## About Task

Clone the project from bitbucket and setup the .env config file according to your local environment.After the setup hit <b>php artisan migrate</b> command to populate the database.

Now simply hit the <b>php artisan serve</b> command to run the project on local.


## Task Details

- The first method should accept a long URL and it should then return a short URL code instead. If the URL has already been shortened the same short URL code should be returned.
- The second method should accept the short URL code and return the full URL.
- The third method should accept the short URL code and return the number of times it has been looked up.

<b>Task 1</b> 
Enter the long url and should return short URL. if the URL is already been shortend then should return the same URL.

Enter the URL in the below Field.
![Enterurl](public/img/enterurl.png?raw=true "Title")

Then hit the short URL butto which will return the short URL.

![Shorturl](public/img/shorturl.png?raw=true "Title")

As visible it has returned the short URL. Now try entering the same URL again and it will return the same URL.

<b>Task 2 & Task 3</b>

Now simply click on the short URL visible under the text field and it will return the long url and the count that how many time the short URL has been looked up.

![Shorturl](public/img/longurlandcount.png?raw=true "Title")

Copy the short url and try entering in the browser then it will always return the long URL and the count of short url has been looked up.

![Shorturl](public/img/countincrease.png?raw=true "Title")

Sorry about the design but it was the quickest way possible.

<b>HAPPY CODING!!!</b>
